import {Schema, model} from "mongoose";


const ProductSchema = new Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    description: String,
    price: {
        type: Number,
        required: true
    }
}, {
    timestamps: true
})

const Product = model('Product', ProductSchema)

export default Product