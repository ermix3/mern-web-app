import {Schema} from "mongoose";


const CommandeSchema = Schema({
    email: {
        type: String,
        required: true
    },
    products: {
        type: [String],
    },
    totalPrice: {
        type: Number
    },
}, {
    timestamps: true
})