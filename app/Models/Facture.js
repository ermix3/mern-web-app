import {model, Schema} from "mongoose";


const FactureSchema = Schema({
    email: {
        type: String,
        required: true
    },
    products: {
        type: [String],
    },
    totalPrice: {
        type: Number
    },
}, {
    timestamps: true
})

const Facture = model('facture', FactureSchema)

export default Facture