import {Schema, model} from "mongoose";


// const UserSchema = new Schema({
const UserSchema = Schema({
    username: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        max: 50,
        unique: true
    },
    password: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

const User = model('User', UserSchema)

export default User