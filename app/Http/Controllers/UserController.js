import asyncHandler from "express-async-handler";
import User from '../../Models/User.js'
import bcrypt from "bcryptjs";

export const getUsers = asyncHandler(async (req, res) => {
    const users = await User.find({})
    // console.log(users)
    res.status(200).json({message: "All Users", users})
})

export const getUser = asyncHandler(async (req, res) => {
    // const user = await User.findOne({_id: req.body.id})
    const user = await User.findById(req.params.id)
    res.status(200).json({user})
})

export const addUser = asyncHandler(async (req, res) => {
    const {username, email, password} = req.body
    // @desc Insert single data 1rst way
    // const user = await User.create({username, email, password})
    // @desc Insert single data 2nd way
    // const salt = await bcrypt.genSalt(10)
    // const hashPW = await bcrypt.hash(password, salt)
    // await bcrypt.hash(password, await bcrypt.genSalt(10))
    const newUser = new User({
        username,
        email,
        password: await bcrypt.hash(password, await bcrypt.genSalt(10))
    })
    const user = await newUser.save()
    // @desc Insert Many Data
    // const user = await User.insertMany(req.body)
    res.status(201).json({message: "User created successfully!!", user})
})

export const updateUser = asyncHandler(async (req, res) => {
    const {id} = req.params
    try {
        const user = await User.findById(id)
        const {username, email, password} = req.body
        user.username = username || user.username
        user.email = email || user.email
        user.password = password || user.password
        user.save()
        // const userUpdated = await User.updateOne({_id: id}, {username, email, password})
        res.status(200).json({message: "User updated successfully!!", user})
    } catch (err) {
        return res.status(404).json({message: 'User not found'});
    }
})

export const deleteUser = asyncHandler(async (req, res) => {
    // const user = await User.deleteOne({_id: req.params.id}) // mogodb method
    // const user = await User.findByIdAndRemove(req.params.id)
    // const user = await User.findByIdAndDelete(req.params.id)
    const user = await User.findOneAndDelete(req.params.id)
    res.status(200).json({message: "User deleted successfully!!", user})
})