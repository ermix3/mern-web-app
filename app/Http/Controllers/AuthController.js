import asyncHandler from "express-async-handler";
import User from "../../Models/User.js";
import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";

export const signIn = asyncHandler(async (req, res) => {
    const {email, password} = req.body
    // console.log(req.body)
    try {
        const user = await User.findOne({email})
        if (!user) {
            return res.status(200).json({message: "User Not Found"})
        }

        if (await bcrypt.compare(password, user.password)) {
            return res.status(200).json({
                userId: user._id,
                token: jwt.sign(
                    {userId: user._id},
                    process.env.JWT_SECRET,
                    {expiresIn: '10d'}
                )
            })
            // const token = jwt.sign(
            //     {userId: user._id},
            //     process.env.JWT_SECRET,
            //     {expiresIn: '10s'}
            // )
            // return res.cookie('jwt', token, {
            //     httpOnly: true,
            //     sameSite: 'strict',
            //     maxAge: 30 * 24 * 60 * 60 * 100
            // })
        }

        res.status(401).json({"message": "Password Incorrect"});
    } catch (e) {
        console.log(e)
        res.status(402).json({"message": "User Not Found"});
    }
})

export const signOut = asyncHandler(async (req, res) => {
    res.status(200).json({message: "User Logged Out.", token: ""})
})

export const signUp = asyncHandler(async (req, res) => {
    const {username, email, password} = req.body
    try {
        const userOld = await User.findOne({email})
        if (userOld) {
            return res.status(422).json({message: "User exists!!"})
        }
        const newUser = new User({
            username,
            email,
            password: await bcrypt.hash(password, await bcrypt.genSalt(10))
        })
        const user = await newUser.save()
        res.status(200).json({
            userId: user._id,
            token: jwt.sign(
                {userId: user._id},
                process.env.JWT_SECRET,
                {expiresIn: '10d'}
            )
        })
    } catch (e) {
        console.log(e)
        res.status(402).json({"message": "User Not Found"});
    }
})