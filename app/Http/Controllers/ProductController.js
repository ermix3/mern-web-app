import asyncHandler from "express-async-handler";
import Product from "../../Models/Product.js";


export const getProducts = asyncHandler(async (req, res) => {
    const products = await Product.find({})
    res.status(200).json({
        message: "All Products",
        products
    })
})

export const getProduct = asyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id)
    res.status(200).json({
        message: "The Product",
        product
    })
})

export const addProduct = asyncHandler(async (req, res) => {
    const {name, description, price} = req.body
    const product = await Product.create({name, description, price})
    res.status(200).json({
        message: "Product Created successfully!!",
        product
    })
})

export const updateProduct = asyncHandler(async (req, res) => {
    const {name, description, price} = req.body
    const product = await Product.findByIdAndUpdate(req.params.id, {name, description, price})
    res.status(200).json({
        message: "Product Updated successfully!!",
        product
    })
})

export const deleteProduct = asyncHandler(async (req, res) => {
    const {name, description, price} = req.body
    const product = await Product.findByIdAndDelete(req.params.id)
    res.status(200).json({
        message: "Product Deleted successfully!!",
        product
    })
})