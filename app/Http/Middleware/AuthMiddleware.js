import asyncHandler from "express-async-handler";
import User from "../../Models/User.js";
import jwt from "jsonwebtoken";


export const isAuthenticated = asyncHandler(async (req, res, next) => {
    // console.log(req.cookie)
    // const {token} = req.cookies;
    // if (!token) {
    //     return next('Please Login In')
    // }
    //
    // const verify = await jwt.verify(token, process.env.JWT_SECRET)
    //
    // req.user = await User.findById(verify.id)
    // next()

    // other way
    console.log(req.headers)
    try {
        const token = req.headers['authorization']?.split(' ')[1];
        const verify = await jwt.verify(token, process.env.JWT_SECRET)
        req.user = await User.findById(verify.id)
    } catch (e) {
        console.log(e)
        res.status(401).json({message: "unauthorised!!!"})
    }
    next()
})