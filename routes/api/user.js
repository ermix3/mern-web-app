import express from "express";
import {
    signIn,
    signOut,
    signUp
} from "../../app/Http/Controllers/AuthController.js";
import {
    getUsers,
    getUser,
    addUser,
    updateUser,
    deleteUser
} from "../../app/Http/Controllers/UserController.js"
import {isAuthenticated} from "../../app/Http/Middleware/AuthMiddleware.js";

const userRouter = express.Router()

// Auth routes
userRouter.route('/sign-up').post(signUp)
userRouter.route('/sign-in').post(signIn)
userRouter.route('/sign-out').post(isAuthenticated, signOut)

// Users routes
userRouter.route('/users').get(isAuthenticated, getUsers)
userRouter.route("/users").post(addUser)
userRouter.route('/users/:id').get(getUser).put(updateUser)
userRouter.route("/users/:id").delete(deleteUser)

export default userRouter