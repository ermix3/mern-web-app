import express from "express";
import {
    addProduct,
    deleteProduct,
    getProduct,
    getProducts,
    updateProduct
} from "../../app/Http/Controllers/ProductController.js";

const productRouter = express.Router()

// Products routes
productRouter.route('/products').get(getProducts)
productRouter.route("/products/").post(addProduct)
productRouter.route('/products/:id').get(getProduct).put(updateProduct)
productRouter.route("/products/:id").delete(deleteProduct)

export default productRouter