import express from 'express'
import connectDB from "./config/db.js";
import productRouter from "./routes/api/product.js";
import userRouter from "./routes/api/user.js";
// import routerWeb from "./routes/web/product.js";
// import {errorHandler, notFound} from "./app/Http/Middleware/ErrorMiddleware.js";

// Connnect to DB
connectDB()

const app = express()
const PORT = process.env.PORT || 9000

// Middleware
// app.use(notFound)
// app.use(errorHandler)
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// Routes
app.get('/', (req, res) => res.send('home'))
app.use('/api/v1', [productRouter, userRouter])
// app.use('/api/v1', userRouter)
// app.use('/',routerWeb)

app.listen(PORT, _ => {
    console.log(`The server is running http://localhost:${PORT}`)
})