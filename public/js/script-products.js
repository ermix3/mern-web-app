// const baseUrl = "http://localhost:5000"
const baseUrl = "http://127.0.0.1:5000"


fetch(baseUrl + "/api/v1/products", {"mode": "no-cors"})
    .then(res => {
        // console.log(res)
        return res.json()
    })
    .then(data => console.log(data))
    .catch(err => console.log(err))